# 显卡  ： 负责图形处理和显示

显卡性能的评判标准-------》1. 流处理器 CUDA（显卡的画师）数量越多，画的越多，帧数（FPS）也越高

    2. 显卡的架构（画画水平） 
    2. 显卡的频率（频率越高，显卡性能越高）

# 显存

显卡性能的评判标准-------》1. 显存频率 （表示小车每秒能运多少次货物）

2.显存位宽（表示小车每次运多少数据）

3.显存容量（仓库大小）

## 显卡分为三大类：独立显卡，集成显卡，核心显卡

集成显卡：集成到主板

核心显卡：集成到CPU内部



## 显卡两大阵营：英伟达 （N卡）    AMD （A卡）

## 英伟达 分为 GTX   RTX（从20代开始）     RTX：光线追踪技术（可开关）

## GTX   10    8   0   Ti                                           T = pro  (加强版)

   英        几    高             后

   伟        代    断             缀

   达                               

## AMD   称为RX

## RX      7   900      XT

   A        几      大          后

   M        代      就         缀

   D                  好



   # 内存 ： 临时存放数据的地方（是CPU的首席助理）

关机状态内存没有数据，内存利用电容存储

内存性能的评判标准-------》1. 容量

2. 频率                           同频下，时序小才是好
3. 时序

## 外存： 是指各种辅助存储器，也叫硬盘



# 硬盘 ： 数据仓库管理员

## 硬盘分为：机械硬盘（HDD）和固态硬盘（SSD）

机械硬盘-------磁盘             固态硬盘----------依靠电子交互

## 接口：物理接口

M.2 接口：可以跑PCI-E通道或者SATA通道

通道：模拟高速接口，速率上限不同

协议：是一种为高效管理数据传输而制定的规则和工作模式



## 显存类型，指**优劣和容量大小**。 显存是主板上显卡上的关键核心部件之一，它的优劣和容量大小会直接关系到显卡的最终性能表现。 可以说显示芯片决定了显卡所能提供的功能和其基本性能，而显卡性能的发挥则很大程度上取决于显存。

## 内存的时序其实就是内存的反应时间，当内存收到CPU发来的指令后，多长时间做出反应，这就是内存的时序。要想反应的越快，时序就要越短。

## 内存的颗粒分为原片、白片、黑片三种，内存在生产的过程中会进行两次不同的检测。第一次检测是颗粒厂商行业规定的标准检测，第二次检测是颗粒厂商自己定制的检测规格，颗粒厂商制定的检测规则要比行业标准检测规则严格的多。当颗粒生产出来时，首先要进行的是行业标准检测，检测合格会进行下一项检测，下一项检测也合格，就会打上颗粒型号跟制造商logo进行售卖，这就是大家常说的原片。白片是第一项检测合格，第二项检测不合格，也会打上颗粒型号，但并不会打上颗粒制造商logo。如果遇到颗粒上的logo并不是三星、镁光、海力士的logo，那么这个颗粒就可能是白片。黑片，则是第一次检测就不合格，本应该报废处理，但总会被资本家通过各种方式和渠道流入市场。













